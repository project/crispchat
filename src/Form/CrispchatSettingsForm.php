<?php

namespace Drupal\crispchat\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class CrispchatSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'crispchat.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'crispchat_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['website_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your website ID'),
      '#description' => $this->t('Visit <a href="https://crisp.chat" target="_blank">https://crisp.chat</a> to get your website id.'),
      '#default_value' => $this->config('crispchat.settings')
        ->get('website_id'),
      '#require' => TRUE,
    ];

    $form['disabled_on_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Do not display the widget on admin pages.'),
      '#default_value' => $this->config('crispchat.settings')
        ->get('disabled_on_admin'),
    ];

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->config('crispchat.settings')
        ->get('enabled'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    foreach (['enabled', 'website_id', 'disabled_on_admin'] as $key) {
      $value = $form_state->getValue($key);
      $this->config('crispchat.settings')
        ->set($key, $value);
    }
    $this->config('crispchat.settings')->save(TRUE);

    $this->messenger()->addStatus($this->t('Configuration saved.'));
  }

}
