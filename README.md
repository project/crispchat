CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration

 INTRODUCTION
------------
The crisp module allow you to implement crisp.chat in your Drupal website.
For more information visit https://crisp.chat/.


REQUIREMENTS
------------
This module doesn't require any additional modules.

INSTALLATION
------------
 * Install as usual. See https://www.drupal.org/docs/8/extending-drupal-8/installing-modules for further information.

CONFIGURATION
-------------
 * Go to configuration page and set your site ID.
